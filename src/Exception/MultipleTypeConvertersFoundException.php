<?php

namespace Bleicker\Converter\Exception;

use Bleicker\Exception\ThrowableException as Exception;

/**
 * Class MultipleTypeConvertersFoundException
 *
 * @package Bleicker\Converter\Exception
 */
class MultipleTypeConvertersFoundException extends Exception {

}
